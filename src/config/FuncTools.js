export function shuffleArray(ar) {
  let array = ar.slice();
  for (let i = array.length - 1; i > 0; i--) {
    let j = Math.floor(Math.random() * (i + 1));
    let temp = array[i];
    array[i] = array[j];
    array[j] = temp;
  }
  return array;
}

export function replaceElementInArray(array, item, replacement) {
  let index = array.indexOf(item);
  let updatedArray = array.slice();
  if (index !== -1) {
    updatedArray[index] = replacement;
  }
  return updatedArray;
}
