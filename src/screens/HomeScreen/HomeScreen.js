import React from 'react';
import {FlatList, TouchableOpacity, View, Text, Button} from 'react-native';

import {styles} from './HomeScreen.styles';
import useHomescreenHook from './HomeScreenHook';
const HomeScreen = props => {
  const [
    data,
    firstSelection,
    moves,
    matches,
    secondSelection,
    onCardClick,
    onRestart,
  ] = useHomescreenHook();
  const renderContent = () => {
    return (
      <View style={styles.container}>
        <FlatList data={data} numColumns={4} renderItem={renderItem} />
        {renderBottomSection()}
      </View>
    );
  };
  const renderItem = ({item, index}) => {
    if (item === '') {
      return renderBlankView();
    } else {
      return renderCard({item, index});
    }
  };
  const renderCard = ({item, index}) => {
    return (
      <TouchableOpacity
        onPress={() => {
          onCardClick(item, index);
        }}>
        <View style={styles.cardStyle}>
          <Text>
            {index === firstSelection || index === secondSelection
              ? item
              : 'MG'}
          </Text>
        </View>
      </TouchableOpacity>
    );
  };
  const renderBlankView = () => {
    return <View style={styles.blankView} />;
  };
  const renderBottomSection = () => {
    return (
      <View>
        <View style={styles.flexStyle}>
          <Text style={styles.valuesStyle}>{`Moves - ${moves}`}</Text>
          <Text style={styles.valuesStyle}>{`Matches - ${matches}`}</Text>
        </View>
        <View style={styles.btnStyle}>
          <Button title={'Restart'} onPress={onRestart} />
        </View>
      </View>
    );
  };

  return renderContent();
};

export default HomeScreen;
