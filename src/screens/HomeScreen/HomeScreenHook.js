import {useEffect, useState} from 'react';
import {shuffleArray, replaceElementInArray} from '../../config/FuncTools';
import {dataArray} from '../../config/Constants';

const useHomescreenHook = () => {
  const [data, setData] = useState([]);
  const [firstSelection, setFirstSelection] = useState(-1);
  const [secondSelection, setSecondSelection] = useState(-1);
  const [moves, setMoves] = useState(0);
  const [matches, setMatches] = useState(0);
  const [pairs, setPairs] = useState(0);

  useEffect(() => {
    let temp = shuffleArray(dataArray);
    setData(temp);
    console.log(temp);
  }, []);
  const onRestart = () => {
    setData(shuffleArray(dataArray));
    setMatches(matches + 1);
    setFirstSelection(-1);
    setSecondSelection(-1);
  };
  const onCardClick = (item, index) => {
    if (firstSelection === -1) {
      setFirstSelection(index);
    } else if (secondSelection === -1) {
      setSecondSelection(index);
      setMoves(moves + 1);
      setTimeout(() => {
        if (data[firstSelection] !== item) {
          setFirstSelection(-1);
          setSecondSelection(-1);
        } else {
          let temp = replaceElementInArray(data, item, '');
          temp = replaceElementInArray(temp, item, '');
          setData(temp);
          setFirstSelection(-1);
          setSecondSelection(-1);
          setPairs(pairs + 1);
          if (pairs === 8) {
            onRestart();
          }
        }
      }, 200);
    }
  };
  return [
    data,
    firstSelection,
    moves,
    matches,
    secondSelection,
    onCardClick,
    onRestart,
  ];
};

export default useHomescreenHook;
