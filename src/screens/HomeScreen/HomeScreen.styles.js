import {StyleSheet, Dimensions} from 'react-native';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    paddingHorizontal: 10,
    paddingTop: 80,
  },
  cardStyle: {
    width: (Dimensions.get('window').width - 100) / 4,
    elevation: 3,
    marginHorizontal: 10,
    backgroundColor: 'white',
    height: 50,
    alignItems: 'center',
    marginVertical: 20,
    justifyContent: 'center',
  },
  blankView: {
    width: (Dimensions.get('window').width - 100) / 4,
    marginHorizontal: 10,
    backgroundColor: 'transparent',
    height: 50,
    borderWidth: 1,
    marginVertical: 20,
  },
  valuesStyle: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  btnStyle: {
    marginVertical: 40,
  },
  flexStyle: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: 20,
  },
});
